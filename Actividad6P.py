'''
Created on 2 oct. 2020

@author: Denise
'''
import array

class Aspirante:
    
    def __init__(self, folio, nombre, edad, redesSoc):
        if folio is None:
            self.__folio = int()
        else:
            self.__folio = folio
        
        if nombre is None:
            self.__nombre = str("")
        else:
            self.__nombre = nombre
        
        if edad is None:
            self.__edad = 0
        else:
            self.__edad = edad
            
        if redesSoc is None:
            self.__redesSoc = []
        else: 
            self.__redesSoc = redesSoc
        
        
    def getFolio(self):
        return self.__folio
    def setFolio(self, folio):
        self.__folio = folio
    
    def getNombre(self):
        return self.__nombre
    def setNombre(self, nombre):
        self.__nombre = nombre
        
    def getEdad(self):
        return self.__edad
    def setEdad(self, edad):
        self.__edad = edad
        
    def getRedesSoc(self):
        return self.__redesSoc
    def setRedesSoc(self, redesSoc):
        self.__redesSoc = redesSoc
        
    def mostrar(self):
        print("Folio: " + str(self.getFolio()) + ", Nombre: " + self.getNombre() + ", Edad: " + str(self.getEdad()) + ", Redes Sociales: [", end = "")
        print(*self.getRedesSoc(), sep = ", ", end = "]")
        
class RegistroAspirantes:
    
    def __init__(self, listaAsp, numFolio):
        if listaAsp is None:
            self.__listaAsp = []
        else:
            self.__listaAsp = listaAsp
            
        if numFolio is None: 
            self.__numFolio = 1
        else:
            self.__numFolio = numFolio
            
    def getListaAsp(self):
        return self.__listaAsp
    def setListaAsp(self, listaAsp):
        self.__listaAsp = listaAsp
        
    def getNumFolio(self):
        return self.__numFolio
    def setNumFolio(self, numFolio):
        self.__numFolio = numFolio
        
    def validacion(self):
        error = 1
        while error == 1:
            try:
                r = int(input())
            except: 
                print("Solo numeros enteros, intentelo de nuevo: ")
                error = 1
            else: 
                if r > 0:
                    error = 0
                else: 
                    print("Solo numero enteros, intentelo de nuevo: ")
                    error = 1
        return r
    
    def agregarAsp(self, a):
        self.setNumFolio(self.getNumFolio() + 1)
        Aspirante = self.getListaAsp()
        Aspirante.append(a)
        self.setListaAsp(Aspirante)
        
    def eliminarAsp(self, folio):
        self.setNumFolio(self.getNumFolio() - 1)
        Aspirante = self.getListaAsp()
        Aspirante.pop()
        self.setListaAsp(Aspirante)
        
    def mostrarAsp(self):
        asp = self.getListaAsp()
        for i in asp:
            Aspirante.mostrar(i)
            print()
        print()
    
    
regAsp = RegistroAspirantes([],1) 
nombre = "" 
folio = ""
opcion = 0
edad = 0

while opcion!=4:
    print("1- Agregar Aspirante")
    print("2- Eliminar Aspirante")
    print("3- Mostrar Aspirantes")
    print("4- Salir del menu")
    opcion = regAsp.validacion()
    
    if opcion == 1:
        redesSoc = []
        nombre = str(input("Ingrese el Nombre: "))
        print("Ingrese la edad: ")
        edad = regAsp.validacion()
        redesSoc.append(input("Instagram: "))
        redesSoc.append(input("Facebook: "))
        redesSoc.append(input("Twitter: "))
        redesSoc.append(input("Snapchat: "))
        
        if len(regAsp.getListaAsp()) == 0:
            regAsp.agregarAsp(Aspirante(1, nombre, edad, redesSoc))
        else:
            regAsp.agregarAsp(regAsp.getNumFolio(), nombre, edad, redesSoc)
    elif opcion == 2:
        regAsp.eliminarAsp(folio)
    elif opcion == 3:
        regAsp.mostrarAsp()
    elif opcion == 4:
        pass
    else: 
        print("Esa opcion no se encuentra dentro del menu, intentelo de nuevo")
        
 
            